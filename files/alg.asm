section .data
  curr:
      word 1
  next:
      word 2
  prev:
      word 1
  sum:
      word 0
  section .text
  start:
      ld reg1, curr
      ld reg2, next
      ld reg3, prev
      ld reg4, sum
  startif:
      mod reg0, reg1, 2
      beq addsum
  iteration:
      add reg2, reg2, reg1
      add reg1, reg1, reg3
      sub reg3, reg2, reg1 
      sub reg0, reg1, 4000000
      bne startif
      print reg4
      hlt
  addsum:
      add reg4, reg4, reg1
      jmp iteration